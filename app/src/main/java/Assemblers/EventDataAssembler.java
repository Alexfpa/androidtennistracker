package Assemblers;

import HistoryModel.Event;
import ModelDao.EventData;

public class EventDataAssembler {

    /**
     * Convert Event object into an EventData object
     * @param event
     * @return
     */
    public static EventData getEventData(Event event){

        //Exchange to String
        String exchange = ((event.getServeType() != null) ? event.getServeType().getLabel2() : "-")
                + "/"
                + ((event.getExchangeType() != null) ? event.getExchangeType().getPlayer()+":"+event.getExchangeType().getLabel2() : "-");

        //Server
        String server = event.getServerPlayer().getName();

        //Score
        int serviceScoreFirst = event.getFirstPlayerServiceScore().second;
        int serviceScoreSecond = event.getSecondPlayerServiceScore().second;
        String score = null;
        if(serviceScoreFirst == 50 || serviceScoreSecond == 50)
            score = ((serviceScoreFirst == 50) ? "AV" : serviceScoreFirst) + "-" + ((serviceScoreSecond == 50) ? "AV" : serviceScoreSecond);
        else if(serviceScoreFirst == 60 || serviceScoreSecond == 60)
            score = " ";
        else
            score = serviceScoreFirst + "-"+ serviceScoreSecond;

        //Display match score
        String matchScore = event.getFirstPlayerGameScore().second + "-" + event.getSecondPlayerGameScore().second;

        String dateTimeid = event.getDateTimeId();

        return new EventData(dateTimeid,server,exchange,score,matchScore);
    }
}
