package PersistanceServices;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import Assemblers.EventDataAssembler;
import Controller.DatabasesManager;
import HistoryModel.Event;
import ModelDao.EventData;
import ModelDao.MatchData;

/** This class is a singleton */
public class EventsPersistenceService {

    /** The controller */
    private DatabasesManager controller;

    /** The EventsPersistenceService instance */
    private static EventsPersistenceService instance;

    /**
     * The private constructor
     */
    private EventsPersistenceService(){
        controller = DatabasesManager.getInstance();
    }

    /**
     * Get the instance
     * @return instance
     */
    public static EventsPersistenceService getInstance(){
        if(instance == null)
            instance = new EventsPersistenceService();
        return instance;
    }

    /**
     * Register Events
     * @param dateTimeId
     * @param eventsList
     * @param context
     */
    public void registerEvents(String dateTimeId, List<Event> eventsList, Context context){
         List<EventData> eventDataList = new ArrayList<>();
        for(Event event : eventsList){
            eventDataList.add(EventDataAssembler.getEventData(event));
        }
        controller.insertEventDataList(dateTimeId, eventDataList, context);
    }

    /**
     * Find all events by match id
     * @param dateTimeId
     * @param context
     * @return
     */
    public List<EventData> findAllEventsByMatchId(String dateTimeId, Context context){
        return controller.findAllEventsById(dateTimeId, context);
    }

    /** Insert match */
    public MatchData insertMatch(MatchData matchDataToRegister, Context context){
        return controller.insertMatch(matchDataToRegister, context);
    }

    /** Get matches */
    public void getMatches(Context context){
        controller.getMatches(context);
    }

    /** Get Event */
    public void getEvent(String id, Context context){
        controller.getEvent(id, context);
    }

    public void deleteHistoryRecord(MatchData matchData, Context context){
        controller.deleteHistoryRecord(matchData.getDateTimeId(), context);
    }
}
