package HistoryModel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Set implements Parcelable {

    /** The services history */
    private ArrayList<Event> eventList;

    /** The constructor */
    public Set() {
        this.eventList = new ArrayList<>();
    }

    /**
     * The Parcel constructor
     * @param in
     */
    protected Set(Parcel in) {
        eventList = in.createTypedArrayList(Event.CREATOR);
    }

    /**
     * Write to parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(eventList);
    }

    /**
     * Describe contents
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * The creator
     */
    public static final Creator<Set> CREATOR = new Creator<Set>() {
        @Override
        public Set createFromParcel(Parcel in) {
            return new Set(in);
        }

        @Override
        public Set[] newArray(int size) {
            return new Set[size];
        }
    };

    /**
     * Get the events list
     * @return services history
     */
    public ArrayList<Event> getEventList() {
        return eventList;
    }

    /**
     * Add element to the events list
     * @param event
     */
    public void addEventToList(Event event) {
        this.eventList.add(event);
    }


}
