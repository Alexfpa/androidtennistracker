package HistoryModel;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;

import GameModel.ExchangeType;
import GameModel.Player;
import GameModel.ServeType;

public class Event implements Parcelable {

    /** The server player */
    private Player serverPlayer;

    /** The serve type */
    private ServeType serveType;

    /** The exchange type */
    private ExchangeType exchangeType;

    /** The first player service score */
    private Pair<Player, Integer> firstPlayerServiceScore;

    /** The second player service score */
    private Pair<Player, Integer> secondPlayerServiceScore;

    /** The first player game score */
    private Pair<Player, Integer> firstPlayerGameScore;

    /** The second player game score */
    private Pair<Player, Integer> secondPlayerGameScore;

    /** The dateTimeId/matchId service score */
    private String dateTimeId;

    /**
     * The constructor
     * @param dateTimeId
     * @param serverPlayer
     * @param serveType
     * @param exchangeType
     * @param firstPlayerServiceScore
     * @param secondPlayerServiceScore
     * @param firstPlayerGameScore
     * @param secondPlayerGameScore
     */
    public Event(String dateTimeId, Player serverPlayer, ServeType serveType, ExchangeType exchangeType,
                 Pair<Player, Integer> firstPlayerServiceScore, Pair<Player, Integer> secondPlayerServiceScore,
                 Pair<Player, Integer> firstPlayerGameScore, Pair<Player, Integer> secondPlayerGameScore) {
        this.serverPlayer = serverPlayer;
        this.serveType = serveType;
        this.exchangeType = exchangeType;
        this.firstPlayerServiceScore = firstPlayerServiceScore;
        this.secondPlayerServiceScore = secondPlayerServiceScore;
        this.firstPlayerGameScore = firstPlayerGameScore;
        this.secondPlayerGameScore= secondPlayerGameScore;
        this.dateTimeId = dateTimeId;
    }

    /**
     * The parcelable constructor
     * @param in
     */
    protected Event(Parcel in) {
        serverPlayer = in.readParcelable(Player.class.getClassLoader());
        String serveType = in.readString();
        this.serveType = (serveType != null) ? ServeType.getType(serveType) : null;
        String exchangeType = in.readString();
        this.exchangeType = (exchangeType != null) ? ExchangeType.getType(exchangeType) : null;
        this.firstPlayerServiceScore = new Pair<>((Player) in.readParcelable(Player.class.getClassLoader()), Integer.valueOf(in.readInt()));

        this.secondPlayerServiceScore = new Pair<>((Player) in.readParcelable(Player.class.getClassLoader()), Integer.valueOf(in.readInt()));

        this.firstPlayerGameScore = new Pair<>((Player) in.readParcelable(Player.class.getClassLoader()), Integer.valueOf(in.readInt()));

        this.secondPlayerGameScore = new Pair<>((Player) in.readParcelable(Player.class.getClassLoader()), Integer.valueOf(in.readInt()));

        this.dateTimeId = in.readString();

    }

    /**
     * Write to parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //TODO: check order of written infos
        dest.writeParcelable(serverPlayer, flags);
        dest.writeString((this.serveType != null) ? this.serveType.getLabel() : null);
        dest.writeString((this.exchangeType != null) ? this.exchangeType.getLabel() : null);

        dest.writeParcelable(firstPlayerServiceScore.first,flags);
        dest.writeInt(firstPlayerServiceScore.second);

        dest.writeParcelable(secondPlayerServiceScore.first,flags);
        dest.writeInt(secondPlayerServiceScore.second);

        dest.writeParcelable(firstPlayerGameScore.first,flags);
        dest.writeInt(firstPlayerGameScore.second);

        dest.writeParcelable(secondPlayerGameScore.first,flags);
        dest.writeInt(secondPlayerGameScore.second);

        dest.writeString(dateTimeId);
    }

    /**
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * The creator
     */
    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    /**
     * Get the server player
     * @return
     */
    public Player getServerPlayer() {
        return serverPlayer;
    }

    /**
     * Get the serve type
     * @return
     */
    public ServeType getServeType() {
        return serveType;
    }

    /**
     * Get the exchange type
     * @return the exchange type
     */
    public ExchangeType getExchangeType() {
        return exchangeType;
    }

    /**
     * Get the first player service score
     * @return firstPlayerServiceScore
     */
    public Pair<Player, Integer> getFirstPlayerServiceScore() {
        return firstPlayerServiceScore;
    }

    /**
     * Get the second player service score
     * @return secondPlayerServiceScore
     */
    public Pair<Player, Integer> getSecondPlayerServiceScore() {
        return secondPlayerServiceScore;
    }

    /**
     * Get the first player game score
     * @return firstPlayerGameScore
     */
    public Pair<Player, Integer> getFirstPlayerGameScore() {
        return firstPlayerGameScore;
    }

    /**
     * Get the second player game score
     * @return secondPlayerGameScore
     */
    public Pair<Player, Integer> getSecondPlayerGameScore() {
        return secondPlayerGameScore;
    }

    /**
     * Get the dateTimeId/matchId
     * @return dateTimeId
     */
    public String getDateTimeId() {
        return dateTimeId;
    }
}
