package HistoryModel;

import android.os.Parcel;
import android.os.Parcelable;

import HistoryModel.Set;

public class Game implements Parcelable {

    /** The sets */
    private Set[] sets;

    /** Constructor */
    public Game(){
        this.sets = new Set[3];
        for(int i = 0; i <sets.length; i++)
            sets[i] = new Set();
    }

    /**
     * The constructor
     * @param in
     */
    protected Game(Parcel in) {
        sets = in.createTypedArray(Set.CREATOR);
    }

    /**
     * Write to parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(sets, flags);
    }

    /**
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * The creator
     */
    public static final Creator<Game> CREATOR = new Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[size];
        }
    };

    /**
     * Get the sets
     * @return sets
     */
    public Set getSets(int i) {
        if(i< sets.length)
            return sets[i];
        else
            return null;
    }
}
