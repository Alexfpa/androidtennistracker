package GameManagers;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;

import HistoryModel.Event;
import GameModel.ExchangeType;
import GameModel.MatchTypes;
import GameModel.ServeType;

import java.util.TreeMap;

import HistoryModel.Game;
import GameModel.Player;
import GameModel.SetTypes;

public class GameManager implements Parcelable {

    /** The players */
    private TreeMap<String,Player> players;

    /** The game */
    private Game game;

    /** The current set */
    private int currentSet;

    /** The current server player */
    private Player currentServerPlayer;

    /** The set type */
    private SetTypes setType;

    /** The match type */
    private MatchTypes matchType;

    /** The winner */
    private Player winner;

    /** The first alert showed */
    public static boolean firstServerAlertShowed = false;

    /** The date time id */
    public String dateTimeId;

    /**
     * The constructor
     */
    public GameManager(String dateTimeId, String playerName1, String playerName2){
        this.players = new TreeMap<>();
        this.players.put(playerName1, new Player(playerName1));
        this.players.put(playerName2, new Player(playerName2));
        this.game = new Game();
        this.currentSet = 0;
        this.dateTimeId = dateTimeId;
    }

    /**
     * @param in
     */
    protected GameManager(Parcel in) {
        currentSet = in.readInt();
        currentServerPlayer = in.readParcelable(Player.class.getClassLoader());
        winner = in.readParcelable(Player.class.getClassLoader());
        this.setType = SetTypes.getType(in.readString());
        this.matchType = MatchTypes.getType(in.readString());
        this.players = new TreeMap<>();
        in.readMap(players,Player.class.getClassLoader());
        this.game = in.readParcelable(Game.class.getClassLoader());
        this.dateTimeId = in.readString();
    }

    /**
     * Write to parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(currentSet);
        dest.writeParcelable(currentServerPlayer, flags);
        dest.writeParcelable(winner, flags);
        dest.writeString(this.setType.getLabel());
        dest.writeString(this.matchType.getLabel());
        dest.writeMap(this.players);
        dest.writeParcelable(this.game,flags);
        dest.writeString(this.dateTimeId);
    }

    /**
     * The creator
     */
    public static final Creator<GameManager> CREATOR = new Creator<GameManager>() {
        @Override
        public GameManager createFromParcel(Parcel in) {
            return new GameManager(in);
        }

        @Override
        public GameManager[] newArray(int size) {
            return new GameManager[size];
        }
    };

    /** @return the players */
    public TreeMap<String, Player> getPlayers() {
        return players;
    }

    /** @param players */
    public void setPlayers(TreeMap<String, Player> players) {
        this.players = players;
    }

    /** @return the game */
    public Game getGame() {
        return game;
    }

    /** @param game */
    public void setGame(Game game) {
        this.game = game;
    }

    /** @return the currentSet */
    public int getCurrentSet() {
        return currentSet;
    }

    /** @param currentSet */
    public void setCurrentSet(int currentSet) {
        this.currentSet = currentSet;
    }

    /** @return the current server player */
    public Player getCurrentServerPlayer() {
        return currentServerPlayer;
    }

    /** @param currentServerPlayer */
    public void setCurrentServerPlayer(Player currentServerPlayer) {
        this.currentServerPlayer = currentServerPlayer;
    }

    /**@return the set type */
    public SetTypes getSetType() {
        return setType;
    }

    /**@return the match type */
    public MatchTypes getMatchType() {
        return matchType;
    }

    /** @return the winner */
    public Player getWinner() {
        return winner;
    }

    /** @param winner */
    public void setWinner(Player winner) {
        this.winner = winner;
    }

    /**@return date time id*/
    public String getDateTimeId() {
        return dateTimeId;
    }

    /**
     * Set match and set types
     * @param setType
     * @param matchType
     */
    public void setMatchAndSetTypes(String setType, String matchType){
        this.setType = SetTypes.getType(setType);
        this.matchType = MatchTypes.getType(matchType);
    }

    /**
     * Register winning point event
     * @param playerName
     * @param exchangeType
     * @param serveType
     */
    public void registerWinningPointEvent(String playerName, ExchangeType exchangeType, ServeType serveType){
        addPointsToPlayer(playerName);
        this.createNewEvent(exchangeType,serveType);
        this.manageServiceWinning();
    }

    /**
     * Register ace event
     */
    public void registerAceEvent(){
        this.currentServerPlayer
                .setServicePoints(addPointsToPlayer(this.currentServerPlayer.getName()));
        this.createNewEvent(null, ServeType.ACE);
        manageServiceWinning();
    }

    /**
     * Register double error event
     */
    public void registerDoubleErrorEvent(){
        Player opponent = findOpponent(this.currentServerPlayer);
        opponent.setServicePoints(addPointsToPlayer(opponent.getName()));
        this.createNewEvent(null,ServeType.DOUBLE_ERROR);
        manageServiceWinning();
    }

    /**
     * Register errors event
     * @param authorOfErrorName
     * @param exchangeType
     * @param serveType
     */
    public void registerErrorsEvents(String authorOfErrorName, ExchangeType exchangeType, ServeType serveType){
        //Find the looser by his name
        Player looser = players.get(authorOfErrorName);
        // Find the opponent that won the service
        Player winner = findOpponent(looser);
        winner.setServicePoints(addPointsToPlayer(winner.getName()));
        this.createNewEvent(exchangeType,serveType);
        manageServiceWinning();
    }

    /**
     * Add points to player that won the service
     * @param winnerPlayerName
     * @return
     */
    public int addPointsToPlayer (String winnerPlayerName){
        Player winnerPlayer = players.get(winnerPlayerName);
        int servePoints = winnerPlayer.getServicePoints();
        if(servePoints == 0 || servePoints == 15)
            winnerPlayer.setServicePoints(servePoints + 15);
        else
            winnerPlayer.setServicePoints(servePoints + 10);

        return winnerPlayer.getServicePoints();
    }

    /**
     * Manage the service winning
     */
    public void manageServiceWinning(){
        boolean matchWon = false;
        for(Player player : players.values()){
            // If if the type of match is with advantage
            if(!this.matchType.isAdvantage()) {
                if (player.getServicePoints() == 50) {
                    matchWon = true;
                    player.getMatchesWon()[currentSet]++;
                    if (player.getMatchesWon()[currentSet] >= this.matchType.getMatchesNumber()) {
                        player.wonSet();
                        currentSet++;
                    }
                    if (player.getNbSetsWon() == this.matchType.getSetsNumber())
                        this.winner = player;
                }
            }else{
                // If if the type of match is without advantage
                if ((player.getServicePoints() == 50 && findOpponent(player).getServicePoints() < 40)
                || (player.getServicePoints() == 60 && findOpponent(player).getServicePoints() == 40)){
                    matchWon = true;
                    player.getMatchesWon()[currentSet]++;
                    if (player.getMatchesWon()[currentSet] >= this.matchType.getMatchesNumber()) {
                        player.wonSet();
                        currentSet++;
                    }
                    if (player.getNbSetsWon() == this.matchType.getSetsNumber())
                        this.winner = player;
                }else if(player.getServicePoints() == 50 && findOpponent(player).getServicePoints() == 50){
                    player.setServicePoints((40));
                    findOpponent(player).setServicePoints(40);
                }
            }
        }
        if(matchWon){
            for(Player player : players.values()){
                player.setServicePoints(0);
            }
            // If the match is won, switch the server
            switchCurrentServer();
        }
    }

    /**
     * Crete new event
     * @param exchangeType
     * @param serveType
     */
    public void createNewEvent(ExchangeType exchangeType, ServeType serveType){
        //Create new event
        Event event = new Event(this.dateTimeId, this.currentServerPlayer, serveType, exchangeType,
                new Pair<Player,Integer>(players.firstEntry().getValue(),players.firstEntry().getValue().getServicePoints()),
                new Pair<Player,Integer>(players.lastEntry().getValue(),players.lastEntry().getValue().getServicePoints()),
                new Pair<Player,Integer>(players.firstEntry().getValue(),players.firstEntry().getValue().getMatchesWon()[this.currentSet]),
                new Pair<Player,Integer>(players.lastEntry().getValue(),players.lastEntry().getValue().getMatchesWon()[this.currentSet]));

        // register service history into the game history
        this.game.getSets(currentSet).addEventToList(event);

    }

    /**
     * Set the first server
     * @param playerName
     */
    public void setFirstServer(String playerName){
        currentServerPlayer = players.get(playerName);
    }

    /**
     * Switch the current server
     */
    public void switchCurrentServer(){
        currentServerPlayer = findOpponent(currentServerPlayer);
    }

    /**
     * Find the opponent of a player
     * @param player
     * @return
     */
    public Player findOpponent(Player player){
        Player opponent = null;
        for(Player newPlayer : players.values()){
            if(!newPlayer.getName().equals(player.getName()))
                opponent = newPlayer;
        }
        return opponent;
    }

    /** @return */
    @Override
    public int describeContents() {
        return 0;
    }

}
