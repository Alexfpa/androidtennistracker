package GameModel;

public enum ServeType {

    FIRST_BALL("1ère balle", "1S"),

    SECOND_BALL("2ème balle", "2S"),

    DOUBLE_ERROR("Double faute", "DF"),

    ACE("Ace", "ACE");

    /**
     * The constructor
     * @param label
     */
    ServeType(String label, String label2){

        this.label = label;

        this.label2 = label2;

    }

    /** The label */
    private String label;

    /** The label2 */
    private String label2;

    /** @return label */
    public String getLabel(){
        return this.label;
    }

    /** @return label2 */
    public String getLabel2(){
        return this.label2;
    }

    /**
     * Get type by type string
     * @param type
     * @return
     */
    public static ServeType getType(String type){
        if(type.equals(FIRST_BALL.label))
            return FIRST_BALL;
        else if(type.equals(SECOND_BALL.label))
            return SECOND_BALL;
        else if(type.equals(DOUBLE_ERROR.label))
            return DOUBLE_ERROR;
        else if(type.equals(ACE.label))
            return ACE;
        return null;
    }
}
