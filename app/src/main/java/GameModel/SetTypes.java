package GameModel;

import java.util.ArrayList;

public enum SetTypes {

    NORMAL_SET("Set normal"),

    TIE_BREAK_SEVEN_POINTS("Tie-break en 7 points"),

    SUPER_TIE_BREAK_TEN_POINTS("Super Tie-break en 10 points");

    /** Label */
    private String label;

    /**
     * The constructor
     * @param label
     */
    SetTypes(String label){
        this.label = label;
    }

    /** @return label */
    public String getLabel(){
        return label;
    }

    /** @return all labels */
    public static ArrayList<String> getAllLabels(){
        ArrayList<String> allLabels = new ArrayList<>();
        for(SetTypes setType : SetTypes.values()){
            allLabels.add(setType.getLabel());
        }
        return allLabels;
    }

    /**
     * Get type by type string
     * @param type
     * @return
     */
    public static SetTypes getType(String type){
        if(type.equals(NORMAL_SET.label))
            return NORMAL_SET;
        else if(type.equals(TIE_BREAK_SEVEN_POINTS.label))
            return TIE_BREAK_SEVEN_POINTS;
        else if(type.equals(SUPER_TIE_BREAK_TEN_POINTS.label))
            return SUPER_TIE_BREAK_TEN_POINTS;
        return null;
    }
}
