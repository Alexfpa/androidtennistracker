package GameModel;

import android.os.Parcel;
import android.os.Parcelable;

public class Player implements Parcelable {

    /** The Name */
    private String name;

    /**The set points */
    private int servicePoints;

    /** The matches won */
    private int matchesWon[];

    /** The sets won */
    private int nbSetsWon;

    /**
     * Constructor
     * @param name
     */
    public Player(String name){
        this.name = name;
        this.servicePoints = 0;
        this.matchesWon = new int[3];
        this.nbSetsWon = 0;
    }

    /**
     * Parcelable Constructor
     * @param in
     */
    protected Player(Parcel in) {
        name = in.readString();
        servicePoints = in.readInt();
        matchesWon = in.createIntArray();
        nbSetsWon = in.readInt();
    }

    /**
     * Write to parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(servicePoints);
        dest.writeIntArray(matchesWon);
        dest.writeInt(nbSetsWon);
    }

    /**
     * Describe contents
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * The Creator
     */
    public static final Creator<Player> CREATOR = new Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };

    /**
     * @return Name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return set points
     */
    public int getServicePoints() {
        return servicePoints;
    }

    /**
     * @param servicePoints
     */
    public void setServicePoints(int servicePoints) {
        this.servicePoints = servicePoints;
    }

    /**
     * @return matches won
     */
    public int[] getMatchesWon() {
        return matchesWon;
    }

    /**
     * @param matchesWon
     */
    public void setMatchesWon(int[] matchesWon) {
        this.matchesWon = matchesWon;
    }

    /**
     * @return nbSetsWon
     */
    public int getNbSetsWon() {
        return nbSetsWon;
    }

    /**
     * Won set
     */
    public void wonSet(){
        this.nbSetsWon++;
    }
}
