package GameModel;

public enum ExchangeType {

    WINNING_POINT("Point gagnant","PG", null),

    UNFORCED_ERROR("Faute directe","FD", null),

    FORCED_ERROR("Faute provoquée", "FP", null);

    /** The constructor
     * @param label
     */
    ExchangeType(String label, String label2, Integer player){
        this.label = label;
        this.label2 = label2;

    }

    /** The label */
    private String label;

    /** The label2 */
    private String label2;

    /** The player */
    private Integer player;

    /** @return label */
    public String getLabel(){
        return this.label;
    }

    /** @return label2 */
    public String getLabel2(){ return this.label2;}

    /** @return the player */
    public Integer getPlayer(){
        return this.player;
    }

    /**
     * Set the player
     * @param player
     */
    public void setPlayer(Integer player){
        this.player = player;
    }

    /**
     * Get exchange type by it's string type
     * @param type
     * @return
     */
    public static ExchangeType getType(String type){
        if(type.equals(WINNING_POINT.label))
            return WINNING_POINT;
        else if(type.equals(UNFORCED_ERROR.label))
            return UNFORCED_ERROR;
        else if(type.equals(FORCED_ERROR.label))
            return FORCED_ERROR;
        return null;
    }
}
