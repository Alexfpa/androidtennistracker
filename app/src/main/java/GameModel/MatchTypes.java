package GameModel;

import java.util.ArrayList;

public enum MatchTypes {

    TWO_SIX_WITH_ADVANTAGE("2 sets, 6 jeux, Avantage, TB 6-6", 2,6, true),

    TWO_FIVE_WITH_ADVANTAGE("2 sets, 5 jeux, Avantage, TB 5-5", 2,5, true),

    TWO_FOUR_WITH_ADVANTAGE("2 sets, 4 jeux, Avantage, TB 4-4", 2,4, true),

    TWO_FIVE_WITHOUT_ADVANTAGE("2 sets, 5 jeux, Pas d'avantage, TB 4-4", 2,5, false),

    TWO_FOUR_WITHOUT_ADVANTAGE("2 sets, 4 jeux, Pas d'avantage, TB 3-3", 2,4, false),

    TWO_THREE_WITHOUT_ADVANTAGE("2 sets, 3 jeux, Pas d'avantage, TB 2-2", 2,3, false);

    /** The label */
    private String label;

    /** The set number */
    private int setsNumber;

    /** The matches number */
    private int matchesNumber;

    /** The advantage */
    private boolean advantage;


    /**
     * The constructor
     * @param label
     */
    MatchTypes(String label, int setsNumber, int matchesNb, boolean advantage){
        this.label = label;
        this.setsNumber = setsNumber;
        this.matchesNumber = matchesNb;
        this.advantage = advantage;
    }

    /** @return label */
    public String getLabel(){
        return this.label;
    }


    /** @return all labels */
    public static ArrayList<String> getAllLabels(){
        ArrayList<String> allLabels = new ArrayList<>();
        for(MatchTypes matchType : MatchTypes.values()){
            allLabels.add(matchType.getLabel());
        }
        return allLabels;
    }

    /**
     * Get matches number
     * @return matchesNumber
     */
    public int getMatchesNumber() {
        return matchesNumber;
    }

    /**
     * Is advantage
     * @return advantage
     */
    public boolean isAdvantage() {
        return advantage;
    }

    /**
     * Get sets number
     * @return setsNumber
     */
    public int getSetsNumber() {
        return setsNumber;
    }

    /**
     * Get MatchType by the type string
     * @param type
     * @return matchType
     */
    public static MatchTypes getType(String type){
        if(type.equals(TWO_SIX_WITH_ADVANTAGE.label))
            return TWO_SIX_WITH_ADVANTAGE;
        else if(type.equals(TWO_FIVE_WITH_ADVANTAGE.label))
            return TWO_FIVE_WITH_ADVANTAGE;
        else if(type.equals(TWO_FOUR_WITH_ADVANTAGE.label))
            return TWO_FOUR_WITH_ADVANTAGE;
        else if(type.equals(TWO_FIVE_WITHOUT_ADVANTAGE.label))
            return TWO_FIVE_WITHOUT_ADVANTAGE;
        else if(type.equals(TWO_FOUR_WITHOUT_ADVANTAGE.label))
            return TWO_FOUR_WITHOUT_ADVANTAGE;
        else if(type.equals(TWO_THREE_WITHOUT_ADVANTAGE.label))
            return TWO_THREE_WITHOUT_ADVANTAGE;
        return null;
    }
}
