package StringConstants;

public class FrStringConstants {
    public static final String NORMAL_SET = "Set normal";

    public static final String TIE_BREAK_SEVEN_POINTS = "Tie-break en 7 points";

    public static final String SUPER_TIE_BREAK_TEN_POINTS = "Super Tie-break en 10 points";

    public static final String FIRST_BALL = "1ère balle";

    public static final String SECOND_BALL = "2ème balle";

    public static final String DOUBLE_ERROR = "Double faute";

    public static final String ACE = "Ace";

    public static final String TWO_SIX_WITH_ADVANTAGE = "2 sets, 6 jeux, Avantage, TB 6-6";

    public static final String TWO_FIVE_WITH_ADVANTAGE = "2 sets, 5 jeux, Avantage, TB 5-5";

    public static final String TWO_FOUR_WITH_ADVANTAGE = "2 sets, 4 jeux, Avantage, TB 4-4";

    public static final String TWO_FIVE_WITHOUT_ADVANTAGE = "2 sets, 5 jeux, Pas d'avantage, TB 4-4";

    public static final String TWO_FOUR_WITHOUT_ADVANTAGE = "2 sets, 4 jeux, Pas d'avantage, TB 3-3";

    public static final String TWO_THREE_WITHOUT_ADVANTAGE = "2 sets, 3 jeux, Pas d'avantage, TB 2-2";

    public static final String WINNING_POINT = "Point gagnant";

    public static final String UNFORCED_ERROR = "Faute directe";

    public static final String FORCED_ERROR = "Faute provoquée";
}
