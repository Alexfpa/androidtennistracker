package StringConstants;

public class EnStringConstants {

    public static final String NORMAL_SET = "Normal set";

    public static final String TIE_BREAK_SEVEN_POINTS = "Tie-break en 7 points";

    public static final String SUPER_TIE_BREAK_TEN_POINTS = "Super Tie-break en 10 points";

    public static final String FIRST_BALL = "First ball";

    public static final String SECOND_BALL = "Second ball";

    public static final String DOUBLE_ERROR = "Double error";

    public static final String ACE = "Ace";

    public static final String TWO_SIX_WITH_ADVANTAGE = "2 sets, 6 games, Advantage, TB 6-6";

    public static final String TWO_FIVE_WITH_ADVANTAGE = "2 sets, 5 games, Advantage, TB 5-5";

    public static final String TWO_FOUR_WITH_ADVANTAGE = "2 sets, 4 game, Avantage, TB 4-4";

    public static final String TWO_FIVE_WITHOUT_ADVANTAGE = "2 sets, 5 game, Without Avantage, TB 4-4";

    public static final String TWO_FOUR_WITHOUT_ADVANTAGE = "2 sets, 4 game, Without Avantage, TB 3-3";

    public static final String TWO_THREE_WITHOUT_ADVANTAGE = "2 sets, 3 game, Without Avantage, TB 2-2";

    public static final String WINNING_POINT = "Winning point";

    public static final String UNFORCED_ERROR = "Unforced error";

    public static final String FORCED_ERROR = "Forced error";
}
