package ModelDao;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class MatchData implements Parcelable {

    /** The match id */
    private int idMatch;

    /** The date time id */
    private String dateTimeId;

    /** The first player's name */
    private String firstPlayer;

    /** The second player's name */
    private String secondPlayer;

    /** The match format */
    private String matchFormat;

    /** The set format */
    private String formatSet;

    /** The longitude */
    private String longitude;

    /** The latitude */
    private String latitude;

    /**
     * contructeur
     * @param firstPlayer
     * @param secondPlayer
     * @param matchFormat
     * @param formatSet
     */
    public MatchData(String firstPlayer, String secondPlayer, String matchFormat, String formatSet){
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.matchFormat = matchFormat;
        this.formatSet = formatSet;
    }

    /**
     * The MatchData parcel constructor
     * @param in
     */
    protected MatchData(Parcel in) {
        idMatch = in.readInt();
        dateTimeId = in.readString();
        firstPlayer = in.readString();
        secondPlayer = in.readString();
        matchFormat = in.readString();
        formatSet = in.readString();
        longitude = in.readString();
        latitude = in.readString();
    }

    /**
     * Write to parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idMatch);
        dest.writeString(dateTimeId);
        dest.writeString(firstPlayer);
        dest.writeString(secondPlayer);
        dest.writeString(matchFormat);
        dest.writeString(formatSet);
        dest.writeString(longitude);
        dest.writeString(latitude);
    }

    /**
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * The creator
     */
    public static final Creator<MatchData> CREATOR = new Creator<MatchData>() {
        @Override
        public MatchData createFromParcel(Parcel in) {
            return new MatchData(in);
        }

        @Override
        public MatchData[] newArray(int size) {
            return new MatchData[size];
        }
    };

    /** Get the match id */
    public int getIdMatch() {
        return idMatch;
    }
    /** Set the match id */
    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }
    /** Get the first player's name */
    public String getFirstPlayer() {
        return firstPlayer;
    }
    /** Set the first player's name */
    public void setFirstPlayer(String firstPlayer) {
        this.firstPlayer = firstPlayer;
    }
    /** Get the second player's name */
    public String getSecondPlayer() {
        return secondPlayer;
    }
    /** Set the second player's name */
    public void setSecondPlayer(String secondPlayer) {
        this.secondPlayer = secondPlayer;
    }
    /** Get the match format */
    public String getMatchFormat() {
        return matchFormat;
    }
    /** Set the match format */
    public void setMatchFormat(String matchFormat) {
        this.matchFormat = matchFormat;
    }
    /** Get format set */
    public String getFormatSet() {
        return formatSet;
    }
    /** Set format set */
    public void setFormatSet(String formatSet) {
        this.formatSet = formatSet;
    }
    /** Get date time id */
    public String getDateTimeId() {
        return dateTimeId;
    }
    /** Set date time id */
    public void setDateTimeId(String dateTimeId) {
        this.dateTimeId = dateTimeId;
    }
    /** Get longitude */
    public String getLongitude() {
        return longitude;
    }
    /** Set longitude */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    /** Get latitude */
    public String getLatitude() {
        return latitude;
    }
    /** Set latitude */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * toString used to display a match
     * @return
     */
    @Override
    public String toString(){
        return firstPlayer + ", " + secondPlayer + ", " + matchFormat + ", "
                + formatSet + ", " + "long: " + longitude + ", " + "lat: " + latitude;
    }

    /**
     * The conversion into a JSONArray
     * @return
     */
    public JSONArray convertToJSONArray(){
        List laListe = new ArrayList();
        laListe.add(dateTimeId);
        laListe.add(firstPlayer);
        laListe.add(secondPlayer);
        laListe.add(matchFormat);
        laListe.add(formatSet);
        laListe.add(longitude);
        laListe.add(latitude);
        return new JSONArray(laListe);
    }
}
