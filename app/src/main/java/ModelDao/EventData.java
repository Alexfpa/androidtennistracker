package ModelDao;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;
public class EventData {
    /** The event id */
    private int idEvent;

    /** The date time id */
    private String dateTimeId;

    /** The server */
    private String server;

    /** The exchange */
    private String exchange;

    /** The score */
    private String score;

    /** The matches score */
    private String matchesScore;
    /**
     * contructeur
     * @param dateTimeId
     * @param server
     * @param exchange
     * @param score
     * @param matchesScore
     */
    public EventData(String dateTimeId, String server, String exchange, String score, String matchesScore){
        this.dateTimeId = dateTimeId;
        this.server = server;
        this.exchange = exchange;
        this.score = score;
        this.matchesScore = matchesScore;
    }

    /** @return */
    public int getIdEvent() {
        return idEvent;
    }

    /** @param idEvent */
    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    /** @return */
    public String getDateTimeId() {
        return dateTimeId;
    }

    /** @param dateTimeId */
    public void setDateTimeId(String dateTimeId) {
        this.dateTimeId = dateTimeId;
    }

    /** @return */
    public String getServer() {
        return server;
    }

    /** @param server */
    public void setServer(String server) {
        this.server = server;
    }

    /** @return */
    public String getExchange() {
        return exchange;
    }

    /** @param exchange */
    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    /** @return */
    public String getScore() {
        return score;
    }

    /** @param score */
    public void setScore(String score) {
        this.score = score;
    }

    /** @return */
    public String getMatchesScore() {
        return matchesScore;
    }

    /** @param matchesScore */
    public void setMatchesScore(String matchesScore) {
        this.matchesScore = matchesScore;
    }


    /**
     * To String (used to display the event)
     * @return
     */
    @Override
    public String toString(){
        return idEvent + ", " + dateTimeId + ", " + server + ", "
                + exchange + ", " + score + ", " + matchesScore;
    }

    /**
     * Convert To JSON Array
     * @return
     */
    public JSONArray convertToJSONArray(){
        List laListe = new ArrayList();
        laListe.add(dateTimeId);
        laListe.add(server);
        laListe.add(exchange);
        laListe.add(score);
        laListe.add(matchesScore);
        return new JSONArray(laListe);
    }

}