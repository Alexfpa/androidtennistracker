package Database;

public interface AsyncResponse {

    /**
     * Process finish
     * @param output
     */
    void processFinish(String output);
}
