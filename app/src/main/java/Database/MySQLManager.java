package Database;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ModelDao.EventData;
import ModelDao.MatchData;
import Controller.DatabasesManager;

public class MySQLManager implements AsyncResponse {

    /** The MySQL Server address */
    private static final String SERVER_ADDR_INSERT = "http://steventang.fr/tennisTracker/newGame.php";

    /** The databases manager */
    private DatabasesManager databasesManager;

    /** The matches */
    private ArrayList<MatchData> matches;

    /** The events */
    private ArrayList<EventData> events;

    /** The context */
    private Context context;

    /**
     *  The constructor
     * @param context
     */
    public MySQLManager(Context context){
        super();
        this.context = context;
    }

    /**
     * The constructor
     * @param event
     * @param context
     */
    public MySQLManager(String event, Context context){
        super();
        this.context = context;
    }

    /**
     * The constructor
     * @param match
     */
    public MySQLManager(Boolean match){
        super();
    }

    /**
     * retour du serveur distant
     * @param output
     */
    @Override
    public void processFinish(String output) {
        Log.d("Serveur", "************"+output);
        //découper le msg recu
        String[] message = output.split("%");

        if (message.length>1){
            if (message[0].equals("enreg")){
                Log.d("enreg", "***********"+message[1]);
            }else if (message[0].equals("enregEvent")){
                Log.d("enregEvent", "***********"+message[1]);
            }else if (message[0].equals("getAllMatchs")) {
                Log.d("getAllMatchs", "***********"+message[1]);
                matches = new ArrayList<MatchData>();
                try {
                    //JSONObject info = new JSONObject(message[1]);
                    JSONArray array = new JSONArray(message[1]);
                    for(int i=0; i<array.length(); i++){
                        try{
                            JSONObject obj = array.getJSONObject(i);
                            MatchData match = new MatchData(obj.getString("firstPlayer"),
                                    obj.getString("secondPlayer"),
                                    obj.getString("formatMatch"),
                                    obj.getString("formatSet"));
                            match.setIdMatch(obj.getInt("idMatch"));
                            match.setDateTimeId(obj.getString("dateTimeId"));
                            match.setLongitude(obj.getString("longitude"));
                            match.setLatitude(obj.getString("latitude"));
                            Log.d("Match : ", "***************"+match.toString());
                            matches.add(match);
                        }catch (JSONException e){
                            Log.d("erreur", "récupération données impossible"+e.getMessage());
                        }
                    }
                    Log.d("MATCHS ACCESDISTANT", "*********"+ matches.get(matches.size()-1).toString());
                    DatabasesManager.getInstance().setMatches(matches, context);
                } catch (JSONException e) {
                    Log.d("erreur", "conversion JSON impossible"+e.getMessage());
                }
            }else if (message[0].equals("getAllEvents")){
                Log.d("getAllEvents", "***********"+message[1]);
                events = new ArrayList<EventData>();
                try {
                    JSONArray array = new JSONArray(message[1]);
                    for(int i=0; i<array.length(); i++){
                        try{
                            JSONObject obj = array.getJSONObject(i);
                            EventData event = new EventData(obj.getString("dateTimeId"),
                                    obj.getString("serveur"),
                                    obj.getString("echange"),
                                    obj.getString("score"),
                                    obj.getString("jeux"));
                            event.setIdEvent(obj.getInt("idEvent"));
                            Log.d("Event : ", "***************"+event.toString());
                            events.add(event);
                        }catch (JSONException e){
                            Log.d("erreur", "récupération données impossible"+e.getMessage());
                        }
                    }
                    if(events != null && events.size() != 0){
                        Log.d("EVENTS ACCESDISTANT", "*********" + events.get(events.size()-1).toString());
                        DatabasesManager.getInstance().setEvents(events, context);
                    }
                } catch (JSONException e) {
                    Log.d("erreur", "conversion JSON impossible"+e.getMessage());
                }
            }else if (message[0].equals("deleteMatch") || message[0].equals("deleteEvent")) {
                DatabasesManager.getInstance().askForDisplayUpdate(context);
                Log.d("Delete done !", "***********" + message[1]);
            }else if (message[0].equals("Erreur !")){
                Log.d("Erreur !", "***********"+message[1]);
            }
    }
    }

    /**
     * Send the request
     * @param operation
     * @param lesDonneesJSON
     */
    public void sendRequest(String operation, JSONArray lesDonneesJSON){
        AccesHTTP accesDonnes = new AccesHTTP();
        //lien de délégation
        accesDonnes.delegate = this;
        //ajout des paramètres
        accesDonnes.addParam("operation", operation);
        accesDonnes.addParam("lesdonnees", lesDonneesJSON.toString());
        //appel au serveur
        accesDonnes.execute(SERVER_ADDR_INSERT);
        Log.d("ACCES DONNEES EXECUTE", "**********");
    }

}
