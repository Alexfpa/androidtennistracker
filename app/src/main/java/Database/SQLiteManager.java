package Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ModelDao.EventData;
import ModelDao.MatchData;

public class SQLiteManager extends SQLiteOpenHelper {

    /** Local database file name */
    private static final String DATABASE_NAME = "TennisTracker.db";

    /** Database version */
    private static final int DATABASE_VERSION = 5;

    /**
     * The Constructor
     * @param context
     */
    public SQLiteManager(Context context){
        super(context, DATABASE_NAME, null , DATABASE_VERSION);
    }

    /**
     * onCreate
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String strSqlMatchs = "create table if not exists matchs ("
                + "idMatch integer primary key autoincrement,"
                + "dateTimeId text not null,"
                + "firstPlayer text not null,"
                + "secondPlayer text not null,"
                + "formatMatch text not null,"
                + "formatSet text not null,"
                + "longitude text not null,"
                + "latitude text not null)";
        db.execSQL(strSqlMatchs);
        String strSqlEvents = "create table if not exists events("
                + "idEvent integer primary key autoincrement,"
                + "dateTimeId text not null,"
                + "serveur text not null,"
                + "echange text not null,"
                + "score text not null,"
                + "jeux text not null)";
        db.execSQL(strSqlEvents);
        Log.i("DATABASE", "onCreate invoke");
    }

    /**
     * onUpgrade
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSql = "drop table matchs";
        db.execSQL(strSql);
        this.onCreate(db);
        Log.i("DATABASE", "onUpgrade invoke");
    }

    /**
     * Insert a new match into SQLite database
     * @param dateTimeId
     * @param firstPlayer
     * @param secondPlayer
     * @param formatMatch
     * @param formatSet
     * @param longitude
     * @param latitude
     */
    public void insertMatch(String dateTimeId, String firstPlayer, String secondPlayer, String formatMatch,
                            String formatSet, String longitude, String latitude){
        firstPlayer = firstPlayer.replace("'", "''");
        String strSql = "insert into matchs (dateTimeId, firstPlayer, secondPlayer, formatMatch, formatSet, longitude, latitude)" +
                " values (" + "'" + dateTimeId + "','" + firstPlayer + "','" + secondPlayer + "','"
                + formatMatch + "','" + formatSet + "','"
                + longitude + "','" + latitude + "')";
        this.getWritableDatabase().execSQL(strSql);
        Log.i("DATABASE", "insertMatch invoked");

    }

    /**
     * find all matches from the database
     * @return
     */
    public List<MatchData> findAllMatches(){
        List<MatchData> matchs = new ArrayList<>();
        String strSql = "select * from matchs";
        Cursor cursor = this.getReadableDatabase().rawQuery(strSql, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            MatchData match = new MatchData(cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5));
            match.setIdMatch(cursor.getInt(0));
            match.setDateTimeId(cursor.getString(1));
            match.setLongitude(cursor.getString(6));
            match.setLatitude(cursor.getString(7));
            matchs.add(match);
            cursor.moveToNext();
        }
        cursor.close();

        return matchs;
    }

    /**
     * Insert an event
     * @param eventData
     */
    public void insertEvent(EventData eventData){
        String strSql = "insert into events (dateTimeId, serveur, echange, score, jeux) values(" +
                "'" + eventData.getDateTimeId() + "','" + eventData.getServer() + "','"
                + eventData.getExchange() + "','" + eventData.getScore() + "','" + eventData.getMatchesScore() +"')";
        this.getWritableDatabase().execSQL(strSql);
        Log.i("DATABASE", "insertEvent invoked");
    }

    /**
     * Find the oldest match that has been put into the database
     * @return
     */
    public MatchData findOldestMatchData(){
        MatchData result = null;
        String strSql = "select * from matchs order by idMatch asc limit 1";
        Cursor cursor = this.getReadableDatabase().rawQuery(strSql, new String[]{});
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            result = new MatchData(cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5));
            result.setIdMatch(cursor.getInt(0));
            result.setDateTimeId(cursor.getString(1));
            result.setLongitude(cursor.getString(6));
            result.setLatitude(cursor.getString(7));
            cursor.moveToNext();
        }
        cursor.close();
        Log.i("DATABASE", "findOldestMatchData invoked");
        return result;
    }

    /**
     * Find match by it's id
     * @param matchId
     * @return
     */
    public MatchData findMatchDataById(String matchId){
        MatchData matchData = null;
        String strSql = "select * from matchs where dateTimeId = ?";
        Cursor cursor = this.getReadableDatabase().rawQuery(strSql, new String[]{matchId});
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            matchData = new MatchData(cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5));
            matchData.setIdMatch(cursor.getInt(0));
            matchData.setDateTimeId(cursor.getString(1));
            matchData.setLongitude(cursor.getString(6));
            matchData.setLatitude(cursor.getString(7));
            cursor.moveToNext();
        }
        cursor.close();
        Log.i("DATABASE", "findMatchDataById invoked");
        return matchData;
    }

    /**
     * Find all events by id
     * @param dateTimeId
     * @return
     */
    public List<EventData> findAllEventsById(String dateTimeId){
        ArrayList<EventData> events = new ArrayList<>();
        String strSql = "select * from events where dateTimeId = ?";
        Cursor cursor = this.getReadableDatabase().rawQuery(strSql, new String[]{dateTimeId});
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            EventData event = new EventData(cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4),
                    cursor.getString(5));
            event.setIdEvent(cursor.getInt(0));
            events.add(event);
            cursor.moveToNext();
        }
        cursor.close();
        return events;
    }

    /**
     * Insert a list of events into the database
     * @param dateTimeId
     * @param eventDataList
     */
    public void insertEvents(String dateTimeId, List<EventData> eventDataList){
        this.getWritableDatabase().delete("events","dateTimeId=?",new String[]{dateTimeId});
        for(EventData eventData : eventDataList){
            insertEvent(eventData);
        }
        Log.i("DATABASE", "insertEvents invoked");
    }

    /**
     * Delete a match from the database by it's id
     * @param matchId
     */
    public void deleteMatch(String matchId){
        this.getWritableDatabase().delete("matchs","dateTimeId=?",new String[]{matchId});
        Log.i("DATABASE", "insertEvents invoked");
    }

    /**
     * Delete all events by the match id
     * @param dateTimeId
     */
    public void deleteEventsByDateTimeId(String dateTimeId){
        this.getWritableDatabase().delete("events","dateTimeId=?",new String[]{dateTimeId});
        Log.i("DATABASE", "insertEvents invoked");
    }


}
