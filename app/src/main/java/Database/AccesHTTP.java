package Database;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/** The access HTTP class */
public class AccesHTTP extends AsyncTask<String, Integer, Long> {

    /** The settings */
    private ArrayList<NameValuePair> settings;

    /** The return answer */
    private String returnAnswer = null;

    /** The delegate */
    public AsyncResponse delegate = null;

    /** The constructor  */
    public AccesHTTP(){
        settings = new ArrayList<NameValuePair>();
    }

    /**
     * Add parameters
     * @param nom
     * @param valeur
     */
    public void addParam(String nom, String valeur){
        settings.add(new BasicNameValuePair(nom, valeur));
    }

    /**
     * Task in background
     * @param strings
     * @return
     */
    @Override
    protected Long doInBackground(String... strings) {
        HttpClient cnxHttp = new DefaultHttpClient();
        HttpPost paramCnx = new HttpPost(strings[0]);

        try {
            //encodage des paramètres
            paramCnx.setEntity(new UrlEncodedFormEntity(settings));
            //connexion et envoie des paramètres et attente de réponse
            HttpResponse reponse = cnxHttp.execute(paramCnx);
            //transformer la réponse
            returnAnswer = EntityUtils.toString(reponse.getEntity());
        } catch (UnsupportedEncodingException e) {
            Log.d("Erreur encodage", "************"+e.toString());
        } catch (ClientProtocolException e) {
            Log.d("Erreur protocole", "************"+e.toString());
        } catch (IOException e) {
            Log.d("Erreur I/O", "************"+e.toString());
        }
        return null;
    }

    /**
     * On post execute
     * @param result
     */
    @Override
    protected void onPostExecute(Long result){
        Log.d("ON POST", "**********");
        delegate.processFinish(returnAnswer.toString());
    }
}
