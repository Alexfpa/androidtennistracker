package fr.android.moi.androidtennistracker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Go to new_match page
     * @param view
     */
    public void newMatch(View view) {
        startActivity(new Intent(this, StartNewMatchActivity.class));
    }

    /**
     * Go to previous_matches page
     * @param view
     */
    public void previousMatches(View view) {
        startActivity(new Intent(this, PreviousMatchesActivity.class));
    }
}
