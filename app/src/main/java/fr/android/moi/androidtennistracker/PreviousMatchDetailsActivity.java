package fr.android.moi.androidtennistracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Controller.DatabasesManager;
import ModelDao.EventData;
import ModelDao.MatchData;
import PersistanceServices.EventsPersistenceService;

public class PreviousMatchDetailsActivity extends AppCompatActivity {

    private MatchData matchData;

    private ArrayList<EventData> eventDataList;

    private List<EventData> list;

    private List<EventData> eventDataL;

    public static Boolean fromSqlLite;

    private final EventsPersistenceService persistenceService = EventsPersistenceService.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_match_details);

        // Get the matchData from previous activity
        Bundle b = (Bundle) this.getIntent().getBundleExtra("bundleToGet");
        this.matchData = (MatchData) b.getParcelable("matchData");

        eventDataL = EventsPersistenceService.getInstance()
                        .findAllEventsByMatchId(matchData.getDateTimeId(), this);
        if(fromSqlLite){
            displayEvents();
        }else{
            persistenceService.getEvent(matchData.getDateTimeId(), this);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("matchData", matchData);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);

        this.matchData = savedInstanceState.getParcelable("matchData");
    }

    public void returnToGameActivity(View view) {
        this.finish();
    }


    public LinearLayout getLinearLayout() {
        // LinearLayout l = new LinearLayout(this,null, R.style.LinearLineHistoryContent);
        LinearLayout.LayoutParams l = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(l);
        return linearLayout;
    }

    public TextView getTextView(String text) {
        TextView tv = new TextView(this);

        TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        tv.setLayoutParams(params);
        tv.setGravity(Gravity.CENTER);

        tv.setText(text);
        return tv;
    }

    public void setEventDataList(){
        eventDataList = DatabasesManager.getInstance().events;
        displayEvents();
    }

    public void displayEvents(){
        list = (eventDataL == null) ? eventDataList: eventDataL;
        //Begin to set the view
        //Set the title
        ((TextView) findViewById(R.id.playersName))
                .setText(this.matchData.getFirstPlayer().toUpperCase()
                        + " vs "
                        + this.matchData.getSecondPlayer().toUpperCase());

        //Set the scroll view
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        LinearLayout ltv = new LinearLayout(this);
        ltv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        ltv.setOrientation(LinearLayout.VERTICAL);

        for (EventData event : list) {
            LinearLayout lt = getLinearLayout();
            //Display set server player
            lt.addView(getTextView(event.getServer()));

            //Display exchange
            lt.addView(getTextView(event.getExchange()));

            //Display service Score
            lt.addView(getTextView(event.getScore()));

            //Display match score
            lt.addView(getTextView(event.getMatchesScore()));
            ltv.addView(lt);
        }
        scrollView.addView(ltv);
    }
}
