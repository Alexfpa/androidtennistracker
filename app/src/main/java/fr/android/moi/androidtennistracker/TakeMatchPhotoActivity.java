package fr.android.moi.androidtennistracker;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TakeMatchPhotoActivity extends AppCompatActivity {
    //constantes
    private static final int RETOUR_PRENDRE_PHOTO = 1;

    private int requestCode;

    private int resultCode;


    //propriétés
    private Button btnPrendrePhoto;

    private Button btnEnreg;

    private ImageView imgPhotoMatch;

    private String photoPath = null;

    private int idMatch;

    private Bitmap image;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.take_match_photo);
        initActivity();

    }

    private void initActivity(){
        btnPrendrePhoto = (Button) findViewById(R.id.btnPrendrePhoto);
        imgPhotoMatch = (ImageView) findViewById(R.id.imgPhotoMatch);
        btnEnreg = (Button) findViewById(R.id.btnEnreg);
        Intent takePhoto = getIntent();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("photoPath", photoPath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        photoPath = savedInstanceState.getString("photoPath");
        //récupérer l'image
        image = BitmapFactory.decodeFile(photoPath);
        //afficher l'image
        imgPhotoMatch.setImageBitmap(image);

    }

    public void prendrePhoto(View view) {
        //créer un intent pour ouvrir une fenêtre et prendre la photo
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //contrôle de l'intent
        if (intent.resolveActivity(getPackageManager()) != null){
            //créer un nom de fichier unique
            String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File photoDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            try {
                File photoFile = File.createTempFile("photo"+time, ".jpg", photoDir);
                //enregistrer le chemin complet
                photoPath = photoFile.getAbsolutePath();
                //créer l'Uri
                Uri photoUri = FileProvider.getUriForFile(TakeMatchPhotoActivity.this,
                        TakeMatchPhotoActivity.this.getApplicationContext().getPackageName()+".provider",
                        photoFile);
                //transfert de l'uri vers l'intent pour enregistrer la photo dans le fichier temp
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                //ouvrir l'activity par rapport à l'intent
                startActivityForResult(intent, RETOUR_PRENDRE_PHOTO);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * retour de l'appel de l'appareil photo startActivityForResult
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        //vérifie le bon code de retour et l'état du retour ok
        if (requestCode==RETOUR_PRENDRE_PHOTO && resultCode==RESULT_OK){
            //récupérer l'image
            image = BitmapFactory.decodeFile(photoPath);
            //afficher l'image
            imgPhotoMatch.setImageBitmap(image);

        }
    }

    public void savePhoto(View view) {
        MediaStore.Images.Media.insertImage(getContentResolver(), image, "match", "Description");
    }

    public void returnToGameActivity(View view){
        this.finish();
    }
}
