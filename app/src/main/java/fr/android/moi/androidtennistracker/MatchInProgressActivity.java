package fr.android.moi.androidtennistracker;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import GameManagers.GameManager;
import GameModel.ExchangeType;
import GameModel.ServeType;
import HistoryModel.Event;
import PersistanceServices.EventsPersistenceService;

public class MatchInProgressActivity extends AppCompatActivity {

    /** The gameManager */
    private GameManager gameManager;

    /**
     * first player name
     */
    private String localFirstPlayerName;

    /**
     * second player name
     */
    private String localSecondPlayerName;

    private AlertDialog alertDialog;

    private ServeType serveType;

    private String dateTimeId;

    private final EventsPersistenceService persistanceService = EventsPersistenceService.getInstance();

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_match_in_progress);
            //Get buttons and set text
            Button firstBall = (Button) findViewById(R.id.firstBall);
            firstBall.setText(ServeType.FIRST_BALL.getLabel());

            Button secondBall = (Button) findViewById(R.id.secondBall);
            secondBall.setText(ServeType.SECOND_BALL.getLabel());

            Button doubleError = (Button) findViewById(R.id.doubleErrorBall);
            doubleError.setText(ServeType.DOUBLE_ERROR.getLabel());

            Button firstBallAce = (Button) findViewById(R.id.aceFirstBall);
            firstBallAce.setText(ServeType.ACE.getLabel());

            Button secondBallAce = (Button) findViewById(R.id.aceSecondBall);
            secondBallAce.setText(ServeType.ACE.getLabel());

            Button firstWinningPoint = (Button) findViewById(R.id.firstWinningPoint);
            firstWinningPoint.setText(ExchangeType.WINNING_POINT.getLabel());

            Button secondWinningPoint = (Button) findViewById(R.id.secondWinningPoint);
            secondWinningPoint.setText(ExchangeType.WINNING_POINT.getLabel());

            Button firstUnforcedError = (Button) findViewById(R.id.firstUnforcedError);
            firstUnforcedError.setText(ExchangeType.UNFORCED_ERROR.getLabel());

            Button secondUnforcedError = (Button) findViewById(R.id.secondUnforcedError);
            secondUnforcedError.setText(ExchangeType.UNFORCED_ERROR.getLabel());

            Button firstForcedError = (Button) findViewById(R.id.firstForcedError);
            firstForcedError.setText(ExchangeType.FORCED_ERROR.getLabel());

            Button secondForcedError = (Button) findViewById(R.id.secondForcedError);
            secondForcedError.setText(ExchangeType.FORCED_ERROR.getLabel());

        String a = Locale.getDefault().getDisplayLanguage();

        //set Player names
            Intent newMatch = getIntent();
            if (newMatch != null) {
                //set id match
                if (newMatch.hasExtra("idMatch")) {
                    dateTimeId = newMatch.getStringExtra("idMatch");
                    Log.d("ID MATCH", "**********"+ dateTimeId);
                }
                TextView firstPlayerName = findViewById(R.id.firstPlayerName);
                TextView secondPlayerName = findViewById(R.id.secondPlayerName);
                if (newMatch.hasExtra("firstPlayer")) {
                    String firstPName = newMatch.getStringExtra("firstPlayer");
                    firstPlayerName.setText(firstPName);
                    localFirstPlayerName = firstPName;
                }
                if (newMatch.hasExtra("secondPlayer")) {
                    String secondPName = newMatch.getStringExtra("secondPlayer");
                    secondPlayerName.setText(secondPName);
                    localSecondPlayerName = secondPName;
                }

                //Initialize the gameManager
                if(savedInstanceState != null && savedInstanceState.getParcelable("gameManager") != null)
                    this.gameManager = (GameManager) savedInstanceState.getParcelable("gameManager");
                else
                    this.gameManager = new GameManager(dateTimeId, firstPlayerName.getText().toString(),
                            secondPlayerName.getText().toString());

                // Initialize match and set types
                if(newMatch.getStringExtra("setFormat") != null
                        && newMatch.getStringExtra("matchFormat") != null)
                    this.gameManager.setMatchAndSetTypes(
                            newMatch.getStringExtra("setFormat"), newMatch.getStringExtra("matchFormat"));
            }

            if (!GameManager.firstServerAlertShowed) {
                this.buildAlertDialog();
                GameManager.firstServerAlertShowed = true;
            }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("firstPlayerName", ((TextView) findViewById(R.id.firstPlayerName)).getText().toString());
        outState.putString("secondPlayerName", ((TextView) findViewById(R.id.secondPlayerName)).getText().toString());

        outState.putString("firstPlayerServiceScore", ((TextView) findViewById(R.id.servicePoints1)).getText().toString());
        outState.putString("firstPlayerSetScore1", ((TextView) findViewById(R.id.firstSetPoints1)).getText().toString());
        outState.putString("firstPlayerSetScore2", ((TextView) findViewById(R.id.secondSetPoints1)).getText().toString());
        outState.putString("firstPlayerSetScore3", ((TextView) findViewById(R.id.thirdSetPoints1)).getText().toString());

        outState.putString("secondPlayerServiceScore", ((TextView) findViewById(R.id.servicePoints2)).getText().toString());
        outState.putString("secondPlayerSetScore1", ((TextView) findViewById(R.id.firstSetPoints2)).getText().toString());
        outState.putString("secondPlayerSetScore2", ((TextView) findViewById(R.id.secondSetPoints2)).getText().toString());
        outState.putString("secondPlayerSetScore3", ((TextView) findViewById(R.id.thirdSetPoints2)).getText().toString());

        outState.putParcelable("gameManager", gameManager);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        ((TextView) findViewById(R.id.firstPlayerName)).setText(savedInstanceState.getString("firstPlayerName"));
        ((TextView) findViewById(R.id.secondPlayerName)).setText(savedInstanceState.getString("secondPlayerName"));

        ((TextView) findViewById(R.id.servicePoints1)).setText(savedInstanceState.getString("firstPlayerServiceScore"));
        ((TextView) findViewById(R.id.firstSetPoints1)).setText(savedInstanceState.getString("firstPlayerSetScore1"));
        ((TextView) findViewById(R.id.secondSetPoints1)).setText(savedInstanceState.getString("firstPlayerSetScore2"));
        ((TextView) findViewById(R.id.thirdSetPoints1)).setText(savedInstanceState.getString("firstPlayerSetScore3"));

        ((TextView) findViewById(R.id.servicePoints2)).setText(savedInstanceState.getString("secondPlayerServiceScore"));
        ((TextView) findViewById(R.id.firstSetPoints2)).setText(savedInstanceState.getString("secondPlayerSetScore1"));
        ((TextView) findViewById(R.id.secondSetPoints2)).setText(savedInstanceState.getString("secondPlayerSetScore2"));
        ((TextView) findViewById(R.id.thirdSetPoints2)).setText(savedInstanceState.getString("secondPlayerSetScore3"));
        gameManager = savedInstanceState.getParcelable("gameManager");
        updateView();
    }

    /**
     * Save game
     */
    public void saveGame(View view) {
        List<Event> eventsList = new ArrayList<>();
        for(int i = 0; i<= this.gameManager.getCurrentSet(); i++){
            eventsList.addAll(this.gameManager.getGame().getSets(i).getEventList());
        }
        persistanceService.registerEvents(dateTimeId,eventsList, this);
    }

    /**
     * Enf the game
     */
    public void endTheGame(View view) {
        //Build alert dialog box
        AlertDialog.Builder endBuilder = new AlertDialog.Builder(MatchInProgressActivity.this);

        endBuilder.setTitle("Etes vous sûr de vouloir quitter?");
        endBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishActivity();
            }
        });
        endBuilder.setNegativeButton("Non", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        //builder.setMessage("Veuillez sélectionner le premier serveur");
        endBuilder.setCancelable(false);

        // Create the Alert dialog
        AlertDialog endAlertDialog = endBuilder.create();
        // Show the Alert Dialog box
        endAlertDialog.show();
    }

    public void registerWinningPointEvent(View view) {
        ExchangeType exchange = ExchangeType.WINNING_POINT;
        if (view.getId() == R.id.firstWinningPoint) {
            exchange.setPlayer(1);
            gameManager.registerWinningPointEvent(localFirstPlayerName, exchange, this.serveType);
        } else if (view.getId() == R.id.secondWinningPoint) {
            exchange.setPlayer(2);
            gameManager.registerWinningPointEvent(localSecondPlayerName, exchange, this.serveType);
        }
        updateView();
    }

    public void registerUnforcedError(View view) {
        ExchangeType exchange = ExchangeType.UNFORCED_ERROR;
        if (view.getId() == R.id.firstUnforcedError) {
            exchange.setPlayer(1);
            gameManager.registerErrorsEvents(localFirstPlayerName, exchange, this.serveType);
        } else if (view.getId() == R.id.secondUnforcedError) {
            exchange.setPlayer(2);
            gameManager.registerErrorsEvents(localSecondPlayerName, ExchangeType.UNFORCED_ERROR, this.serveType);
        }
        updateView();
    }

    public void registerForcedError(View view) {
        ExchangeType exchange = ExchangeType.FORCED_ERROR;
        if (view.getId() == R.id.firstForcedError) {
            exchange.setPlayer(1);
            gameManager.registerErrorsEvents(localFirstPlayerName, exchange, this.serveType);
        } else if (view.getId() == R.id.secondForcedError) {
            exchange.setPlayer(2);
            gameManager.registerErrorsEvents(localSecondPlayerName, ExchangeType.FORCED_ERROR, this.serveType);
        }
        updateView();
    }

    public void registerAceWinning(View view) {
        gameManager.registerAceEvent();
        updateView();
    }

    public void registerDoubleError(View view) {
        gameManager.registerDoubleErrorEvent();
        updateView();
    }


    public void updateView() {
        String firstServicePointsToShow = null;
        String secondServicePointsToShow = null;

        firstServicePointsToShow = (this.gameManager.getPlayers().get(localFirstPlayerName).getServicePoints() < 50) ?
                (this.gameManager.getPlayers().get(localFirstPlayerName).getServicePoints() + "") : "AV";

        secondServicePointsToShow = (this.gameManager.getPlayers().get(localSecondPlayerName).getServicePoints() < 50) ?
                (this.gameManager.getPlayers().get(localSecondPlayerName).getServicePoints() + "") : "AV";

        ((TextView) findViewById(R.id.servicePoints1))
                .setText("" + firstServicePointsToShow);
        ((TextView) findViewById(R.id.servicePoints2))
                .setText("" + secondServicePointsToShow);

        if (gameManager.getCurrentSet() >= 0) {
            ((TextView) findViewById(R.id.firstSetPoints1))
                    .setText("" + this.gameManager.getPlayers().get(localFirstPlayerName).getMatchesWon()[0]);
            ((TextView) findViewById(R.id.firstSetPoints2))
                    .setText("" + this.gameManager.getPlayers().get(localSecondPlayerName).getMatchesWon()[0]);
        }
        if (gameManager.getCurrentSet() >= 1) {
            ((TextView) findViewById(R.id.secondSetPoints1))
                    .setText("" + this.gameManager.getPlayers().get(localFirstPlayerName).getMatchesWon()[1]);
            ((TextView) findViewById(R.id.secondSetPoints2))
                    .setText("" + this.gameManager.getPlayers().get(localSecondPlayerName).getMatchesWon()[1]);
        }
        if (gameManager.getCurrentSet() >= 2) {
            ((TextView) findViewById(R.id.thirdSetPoints1))
                    .setText("" + this.gameManager.getPlayers().get(localFirstPlayerName).getMatchesWon()[2]);
            ((TextView) findViewById(R.id.thirdSetPoints2))
                    .setText("" + this.gameManager.getPlayers().get(localSecondPlayerName).getMatchesWon()[2]);
        }
        ((TextView) findViewById(R.id.serveType))
                .setText("SERVICE: " + gameManager.getCurrentServerPlayer().getName().toUpperCase());

        if (this.gameManager.getWinner() != null){
            this.deactivateAllButtons();
            this.buildWinnerDialog();
        }
        this.resetButtons();
    }

    public void buildAlertDialog() {

        //Build alert dialog box
        AlertDialog.Builder builder = new AlertDialog.Builder(MatchInProgressActivity.this);
        LayoutInflater inflater = getLayoutInflater();

        View view = inflater.inflate(R.layout.alert_dialog, (ViewGroup) findViewById(R.id.alertLayout));

        TextView textView = (TextView) view.findViewById(R.id.textAlert);
        textView.setText("Veuillez sélectionner le premier serveur");

        Button button1 = (Button) view.findViewById(R.id.selectFirstPlayer);
        button1.setText(localFirstPlayerName);

        Button button2 = (Button) view.findViewById(R.id.selectSecondPlayer);
        button2.setText(localSecondPlayerName);

        //builder.setMessage("Veuillez sélectionner le premier serveur");
        builder.setView(view);
        builder.setCancelable(false);

        // Create the Alert dialog
        alertDialog = builder.create();
        // Show the Alert Dialog box
        alertDialog.show();
    }

    public void selectFirstServer(View view) {
        if (view.getId() == R.id.selectFirstPlayer) {
            gameManager.setFirstServer(localFirstPlayerName);
        } else {
            gameManager.setFirstServer(localSecondPlayerName);
        }
        ((TextView) findViewById(R.id.serveType))
                .setText("SERVICE: " + gameManager.getCurrentServerPlayer().getName().toUpperCase());
        alertDialog.dismiss();

    }

    public void setServe(View view) {
        this.resetButtons();
        if (view.getId() == R.id.firstBall) {
            this.serveType = ServeType.FIRST_BALL;
            ((Button) findViewById(R.id.firstBall))
                    .setBackgroundColor(Color.parseColor("#129d4c"));
        } else if (view.getId() == R.id.secondBall) {
            this.serveType = ServeType.SECOND_BALL;
            ((Button) findViewById(R.id.secondBall))
                    .setBackgroundColor(Color.parseColor("#dd7602"));
        }
    }

    public void resetButtons() {
        this.serveType = null;
        ((Button) findViewById(R.id.firstBall)).setBackgroundColor(Color.parseColor("#18cc64"));
        ((Button) findViewById(R.id.secondBall)).setBackgroundColor(Color.parseColor("#f69b35"));

    }

    public void buildWinnerDialog() {

        //Build alert dialog box
        AlertDialog.Builder builder = new AlertDialog.Builder(MatchInProgressActivity.this);
        builder.setTitle(this.gameManager.getWinner().getName() + " has won!!!");
        builder.setPositiveButton("OK", null);

        builder.setCancelable(false);

        // Create the Alert dialog
        alertDialog = builder.create();
        // Show the Alert Dialog box
        alertDialog.show();
    }

    public void deactivateAllButtons(){
        ((Button) findViewById(R.id.firstBall)).setEnabled(false);
        ((Button) findViewById(R.id.secondBall)).setEnabled(false);
        ((Button) findViewById(R.id.doubleErrorBall)).setEnabled(false);
        ((Button) findViewById(R.id.aceFirstBall)).setEnabled(false);
        ((Button) findViewById(R.id.aceSecondBall)).setEnabled(false);
        ((Button) findViewById(R.id.firstWinningPoint)).setEnabled(false);
        ((Button) findViewById(R.id.secondWinningPoint)).setEnabled(false);
        ((Button) findViewById(R.id.firstUnforcedError)).setEnabled(false);
        ((Button) findViewById(R.id.secondUnforcedError)).setEnabled(false);
        ((Button) findViewById(R.id.firstForcedError)).setEnabled(false);
        ((Button) findViewById(R.id.secondForcedError)).setEnabled(false);
    }

    public void takePhotoPage(View view) {
        Intent takePhoto = new Intent(this, TakeMatchPhotoActivity.class);
        startActivity(takePhoto);
    }

    public void showMenu(final View view) {
       //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(MatchInProgressActivity.this, (Button) view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.game_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.history:
                        startHistoryActivity();
                        break;
                }
                return true;
            }
        });
        popup.show();//showing popup menu
    }

    public void startHistoryActivity(){
        Intent historyActivity = new Intent(this, HistoryActivity.class);
        Bundle target = new Bundle();
        target.putParcelable("gameManager", this.gameManager);

        historyActivity.putExtra("bundleToGet", target);
        startActivity(historyActivity);
    }

    public void finishActivity(){
        this.finish();
        GameManager.firstServerAlertShowed = false;
    }
}
