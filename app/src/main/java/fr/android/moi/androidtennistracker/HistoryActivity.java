package fr.android.moi.androidtennistracker;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import HistoryModel.Event;
import GameManagers.GameManager;

public class HistoryActivity extends AppCompatActivity {

    /** The game manager */
    private GameManager gameManager;

    /**
     * On create
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Bundle b = (Bundle) this.getIntent().getBundleExtra("bundleToGet");
        this.gameManager = (GameManager) b.getParcelable("gameManager");

        ((TextView) findViewById(R.id.playersName)).setText(gameManager.getPlayers().firstEntry().getKey().toUpperCase()
                + " vs "
                + gameManager.getPlayers().lastEntry().getKey().toUpperCase());


        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        LinearLayout ltv = new LinearLayout(this);
        ltv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        ltv.setOrientation(LinearLayout.VERTICAL);

        for (int i = 0; i <= this.gameManager.getCurrentSet(); i++) {
            for (Event event : this.gameManager.getGame().getSets(i).getEventList()) {
                LinearLayout lt = getStandardLinearLayoutWithStyleParams();
                lt.addView(getStandardTextViewWithStyleParams(event.getServerPlayer().getName()));

                //Exchange to String
                String exchange = ((event.getServeType() != null) ? event.getServeType().getLabel2() : "-")
                        + "/"
                        + ((event.getExchangeType() != null) ? event.getExchangeType().getPlayer()+":"+event.getExchangeType().getLabel2() : "-");
                lt.addView(getStandardTextViewWithStyleParams(exchange));

                //Score
                int serviceScoreFirst = event.getFirstPlayerServiceScore().second;
                int serviceScoreSecond = event.getSecondPlayerServiceScore().second;
                String score = null;
                if(serviceScoreFirst == 50 || serviceScoreSecond == 50)
                    score = ((serviceScoreFirst == 50) ? "AV" : serviceScoreFirst) + "-" + ((serviceScoreSecond == 50) ? "AV" : serviceScoreSecond);
                else if(serviceScoreFirst == 60 || serviceScoreSecond == 60)
                    score = " ";
                else
                    score = serviceScoreFirst + "-"+ serviceScoreSecond;
                lt.addView(getStandardTextViewWithStyleParams(score));

                //Display match score
                String matchScore = event.getFirstPlayerGameScore().second + "-" + event.getSecondPlayerGameScore().second;
                lt.addView(getStandardTextViewWithStyleParams(matchScore));
                ltv.addView(lt);
            }
        }
        scrollView.addView(ltv);
    }

    /**
     * On Save instance state
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("gameManager", gameManager);
    }

    /**
     * On restore instance state
     * @param savedInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);

        this.gameManager = savedInstanceState.getParcelable("gameManager");
    }

    /**
     * Return to game activity
     * @param view
     */
    public void returnToGameActivity(View view) {
        this.finish();
    }

    /**
     * Get linear layout
     * @return
     */
    public LinearLayout getStandardLinearLayoutWithStyleParams() {
        LinearLayout.LayoutParams l = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(l);
        return linearLayout;
    }

    /**
     * Get text view
     * @param text
     * @return
     */
    public TextView getStandardTextViewWithStyleParams(String text) {
        TextView tv = new TextView(this);

        TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        tv.setLayoutParams(params);
        tv.setGravity(Gravity.CENTER);

        tv.setText(text);
        return tv;
    }
}
