package fr.android.moi.androidtennistracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Locale;

import ModelDao.MatchData;
import Controller.DatabasesManager;
import PersistanceServices.EventsPersistenceService;

public class PreviousMatchesActivity extends AppCompatActivity {

    /** My list view */
    private ListView myListView;

    /** The matchesData list */
    private ArrayList<MatchData> matches;

    /** The persistence service */
    private final EventsPersistenceService persistenceService = EventsPersistenceService.getInstance();

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.previous_matches);

        persistenceService.getMatches(this);

        ((ListView) findViewById(R.id.listPreviousMatches)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MatchData selectedMatch = (MatchData) parent.getItemAtPosition(position);
                launchPreviousMatchDetails(selectedMatch);
              }
        });

        ((ListView) findViewById(R.id.listPreviousMatches)).setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                MatchData selectedMatch = (MatchData) parent.getItemAtPosition(position);
                createConfirmationRequestAlert(selectedMatch);
                return true;
            }
        });
    }

    /**
     * On save instance state
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("matchs", matches);
    }

    /**
     * On restore instance state
     * @param savedInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        myListView = (ListView) findViewById(R.id.listPreviousMatches);

        this.matches = savedInstanceState.getParcelableArrayList("matchs");
        ArrayAdapter adapter = new ArrayAdapter(PreviousMatchesActivity.this,
                android.R.layout.simple_list_item_1, matches);
        myListView.setAdapter(adapter);
    }

    /**
     * Set matches
     */
    public void setMatches(){
        matches = DatabasesManager.matches;
        Log.d("LISTE DE MATCHS", "*******"+ matches.get(0).toString());
        myListView = (ListView) findViewById(R.id.listPreviousMatches);

        ArrayAdapter adapter = new ArrayAdapter(PreviousMatchesActivity.this,
                android.R.layout.simple_list_item_1, matches);
        myListView.setAdapter(adapter);
    }

    /**
     * Launch previous match details activity
     * @param matchData
     */
    public void launchPreviousMatchDetails(MatchData matchData){
        Intent matchDetails = new Intent(this, PreviousMatchDetailsActivity.class);

        Bundle target = new Bundle();
        target.putParcelable("matchData", matchData);

        matchDetails.putExtra("bundleToGet", target);
        startActivity(matchDetails);

    }

    public void createConfirmationRequestAlert(final MatchData matchData) {
        //Build alert dialog box
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if(Locale.getDefault().getDisplayLanguage().contains("English"))
            builder.setTitle("Are you sure you want to delete the item?");
        else if (Locale.getDefault().getDisplayLanguage().contains("French"))
            builder.setTitle("Etes vous sûr de vouloir supprimer cet item?");
        else
            builder.setTitle("Etes vous sûr de vouloir supprimer cet item?");

        // Ask for permission to delete the item
        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                persistenceService.deleteHistoryRecord(matchData, PreviousMatchesActivity.this);
            }
        });
        // Dismiss the alert
        builder.setNegativeButton("Non", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setCancelable(false);

        // Create the Alert dialog
        AlertDialog endAlertDialog = builder.create();
        // Show the Alert Dialog box
        endAlertDialog.show();
    }
}

