package fr.android.moi.androidtennistracker;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import ModelDao.MatchData;
import GameModel.MatchTypes;
import GameModel.SetTypes;
import PersistanceServices.EventsPersistenceService;

public class StartNewMatchActivity extends AppCompatActivity implements LocationListener {

    /** First player name edited */
    private Boolean firstPlayerNameEdited = false;

    /** Second player name edited */
    private Boolean secondPlayerNameEdited = false;

    /** Begin button */
    private Button beginButton = null;

    /** First player name edit text */
    private EditText firstPlayer = null;

    /** Second player name edit text*/
    private EditText secondPlayer = null;

    /** First player name save state */
    private final String FIRST_PLAYER_SAVED_STATE = "firstPlayerName";

    /** Second player name save state*/
    private final String SECOND_PLAYER_SAVED_STATE = "secondPlayerName";

    /** Match spinner save state */
    private final String MATCH_SPINNER_SAVE_STATE = "matchSpinnerSavedState";

    /** Match spinner save state */
    private final String SET_SPINNER_SAVE_STATE = "setSpinnerSavedState";

    /** The instances counter */
    public static int instancesCounter = 0;

    /** The location manager */
    private LocationManager locationManager;

    /** Localisartion permissions id */
    private static final int PERMS_CALL_ID = 1234;

    /** Internet permission id */
    private static final int INTERNET_PERMISSION_ID = 1235;

    /** The map fragment */
    private MapFragment mapFragment;

    /** The google map service */
    private GoogleMap googleMap;

    /** The latitude */
    private double latitude;

    /** The longitude */
    private double longitude;

    /** The match data to register */
    private MatchData matchDataToRegister;

    /** The persistance service */
    private final EventsPersistenceService persistanceService = EventsPersistenceService.getInstance();

    /**
     * On create
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_match);

        //Build match format spinner
        Spinner matchFormatSpinner = (Spinner) findViewById(R.id.matchFormatSpinner);
        ArrayAdapter matchFormatAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, MatchTypes.getAllLabels());
        matchFormatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        matchFormatSpinner.setAdapter(matchFormatAdapter);

        //Build set format spinner
        Spinner setFormatSpinner = (Spinner) findViewById(R.id.setFormatSpinner);
        ArrayAdapter setFormatAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, SetTypes.getAllLabels());
        setFormatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        setFormatSpinner.setAdapter(setFormatAdapter);

        //Building gamers names
        firstPlayer = (EditText) findViewById(R.id.editFirstPlayerName);
        secondPlayer = (EditText) findViewById(R.id.editSecondPlayerName);

        //Get the "start new game" button
        beginButton = (Button) findViewById(R.id.btnBegin);

        //Ads a listener to the edit text first name in order to change the "start new game" button style
        firstPlayer.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                firstPlayerNameEdited = (null != s && s.length() != 0) ? true : false;
                changeButtonBeginStyle();
            }
        });

        //Ads a listener to the edit text second name in order to change the "start new game" button style
        secondPlayer.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                secondPlayerNameEdited = (null != s && s.length() != 0) ? true : false;
                changeButtonBeginStyle();
            }
        });
        instancesCounter++;

        FragmentManager fragmentManager = getFragmentManager();
        mapFragment = (MapFragment) fragmentManager.findFragmentById(R.id.map);

     }

    /**
     * OnSaveInstanceState
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(FIRST_PLAYER_SAVED_STATE, ((EditText) findViewById(R.id.editFirstPlayerName)).getText().toString());
        outState.putString(SECOND_PLAYER_SAVED_STATE, ((EditText) findViewById(R.id.editSecondPlayerName)).getText().toString());
        outState.putInt(MATCH_SPINNER_SAVE_STATE, ((Spinner) findViewById(R.id.matchFormatSpinner)).getSelectedItemPosition());
        outState.putInt(SET_SPINNER_SAVE_STATE, ((Spinner) findViewById(R.id.setFormatSpinner)).getSelectedItemPosition());

    }

    /**
     * OnRestoreInstanceState
     * @param savedInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ((EditText) findViewById(R.id.editFirstPlayerName)).setText(savedInstanceState.getString(FIRST_PLAYER_SAVED_STATE));
        ((EditText) findViewById(R.id.editFirstPlayerName)).setText(savedInstanceState.getString(FIRST_PLAYER_SAVED_STATE));
        ((Spinner) findViewById(R.id.matchFormatSpinner)).setSelection(savedInstanceState.getInt(MATCH_SPINNER_SAVE_STATE));
        ((Spinner) findViewById(R.id.setFormatSpinner)).setSelection(savedInstanceState.getInt(SET_SPINNER_SAVE_STATE));
    }


    /**
     * Changes the style of "Begin" button
     */
    public void changeButtonBeginStyle() {
        if (firstPlayerNameEdited
                && secondPlayerNameEdited
                && !firstPlayer.getText().toString().equals(secondPlayer.getText().toString())) {
            beginButton.setBackgroundColor(Color.parseColor("#B90E00"));
            beginButton.setTextColor(Color.parseColor("#FFFFFF"));
            beginButton.setEnabled(true);
        } else {
            beginButton.setBackgroundColor(Color.parseColor("#E5E7E9"));
            beginButton.setTextColor(Color.parseColor("#000000"));
            beginButton.setEnabled(false);
        }
    }

    /**
     * Start new match
     * @param view
     */
    public void startNewMatch(View view) {
        String firstPlayerName = firstPlayer.getText().toString();
        String secondPlayerName = secondPlayer.getText().toString();
        String formatMatch = ((Spinner) findViewById(R.id.matchFormatSpinner)).getSelectedItem().toString();
        String formatSet = ((Spinner) findViewById(R.id.setFormatSpinner)).getSelectedItem().toString();

        //Register new match to database
        matchDataToRegister = new MatchData(firstPlayerName, secondPlayerName, formatMatch, formatSet);
        matchDataToRegister.setLongitude(Math.round(this.longitude*100)/100.0d +"");
        matchDataToRegister.setLatitude(Math.round(this.latitude*100)/100.0d +"");

        handleInternetPermissions();
        Log.d("ID MATCH EN COURS", "**********" + Integer.toString(matchDataToRegister.getIdMatch()));

        Intent newMatch = new Intent(this, MatchInProgressActivity.class);
        newMatch.putExtra("idMatch", matchDataToRegister.getDateTimeId());
        newMatch.putExtra("firstPlayer", firstPlayerName);
        newMatch.putExtra("secondPlayer", secondPlayerName);
        newMatch.putExtra("matchFormat", formatMatch);
        newMatch.putExtra("setFormat", formatSet);
        startActivity(newMatch);

    }

    /**
     * On resume
     */
    @Override
    protected void onResume() {
        super.onResume();
        checkPermissions();
    }

    /**
     * Check google map permissions
     */
    private void checkPermissions(){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            }, PERMS_CALL_ID);
            return;
        }
        //TODO: check the units
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
        }
        if (locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 0, this);
        }
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        }

        this.loadMap();
    }

    /**
     * On request permission result
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMS_CALL_ID ){
            checkPermissions();
        }else if (requestCode == INTERNET_PERMISSION_ID){
            handleInternetPermissions();
        }
    }

    /**
     * On pause
     */
    @Override
    protected void onPause() {
        super.onPause();
        if(locationManager != null){
            locationManager.removeUpdates(this);
        }
    }

    /**
     * On provider disabled
     * @param provider
     */
    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * On provider enabled
     * @param provider
     */
    @Override
    public void onProviderEnabled(String provider) {

    }

    /** On status changed */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    /**
     * On location changed
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        if(googleMap != null){
            LatLng ltLg = new LatLng(latitude, longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(ltLg));
        }

    }

    /**
     * Load the map
     */
    private void loadMap(){
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                StartNewMatchActivity.this.googleMap = googleMap;
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(StartNewMatchActivity.this.latitude, longitude), 15.0f));
                googleMap.setMyLocationEnabled(true);
            }
        });
    }


    private void handleInternetPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this, new String[]{
                                Manifest.permission.READ_CONTACTS},INTERNET_PERMISSION_ID);
            return ;
        }
        matchDataToRegister = persistanceService.insertMatch(matchDataToRegister, this);

    }

}
