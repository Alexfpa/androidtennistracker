package Controller;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Database.MySQLManager;
import Database.SQLiteManager;
import ModelDao.EventData;
import ModelDao.MatchData;
import fr.android.moi.androidtennistracker.PreviousMatchDetailsActivity;
import fr.android.moi.androidtennistracker.PreviousMatchesActivity;

/** The class is a Singleton */
public class DatabasesManager {

    /** The instance*/
    private static DatabasesManager instance = null;

    /** The matches */
    public static ArrayList<MatchData> matches;

    /** The events */
    public static ArrayList<EventData> events;

    /** The MySQLManager */
    private static MySQLManager mySQLManager;

    /** The SQLiteManager */
    private static SQLiteManager sqLiteManager;

    /** Constructor */
    private DatabasesManager() { }

    /**
     * Singleton instance creation, has to be called in order
     * to be able to use the methods provided by this class
     * @return
     */
    public static DatabasesManager getInstance() {
        if(instance == null) return new DatabasesManager();
        return instance;
    }

    /**
     * Get all matches from MySQLDatabase
     * @param context
     */
    public void getMatches(Context context) {
        Log.d("GET MATCHES", "**********");
        mySQLManager = new MySQLManager(context);
        mySQLManager.sendRequest("getAllMatchs", new JSONArray());
        Log.d("ENVOI", "**********");
    }

    /**
     * Get all events from MySQLDatabase
     * @param dateTimeId
     * @param context
     */
    public void getEvent(String dateTimeId, Context context) {
        Log.d("GET EVENT", "**********");

        mySQLManager = new MySQLManager(dateTimeId, context);
        //récupérer tous les matchs...
        List param = new ArrayList();
        param.add(dateTimeId);
        mySQLManager.sendRequest("getAllEvents", new JSONArray(param));
        Log.d("ENVOI", "**********");
    }

    /**
     * Set matches list into MySQL
     * @param list
     * @param context
     */
    public void setMatches(ArrayList<MatchData> list, Context context) {
        matches = list;
        ((PreviousMatchesActivity) context).setMatches();
        Log.d("MATCH CONTROLE", "********" + matches.get(0).toString());
    }

    /**
     * Set events list into MySQL
     * @param list
     * @param context
     */
    public void setEvents(ArrayList<EventData> list, Context context) {
        events = list;
        ((PreviousMatchDetailsActivity) context).setEventDataList();
        Log.d("SET EVENT", "********" + events.get(0).toString());
    }

    /**
     * Inserts a match into both SQLite and MySQL databases
     * @param matchData
     * @param context
     * @return
     */
    public MatchData insertMatch(MatchData matchData, Context context) {
        //SQLite
        sqLiteManager = new SQLiteManager(context);
        mySQLManager = new MySQLManager(true);

        //FIRST CHECK IF SqlLite Database is full AND IF SO DELETE THE RECORD FROM SQLITE
        int nbElementsOfSQLite = 0;
        List<MatchData> allSQLiteMatchData = sqLiteManager.findAllMatches();
        nbElementsOfSQLite = allSQLiteMatchData.size();

        if (nbElementsOfSQLite >= 5) {
            //Find the matchData that has to be removed
            MatchData matchToMove = sqLiteManager.findOldestMatchData();

            //then delete oldest match and events record from SQLite
            sqLiteManager.deleteMatch(matchToMove.getDateTimeId());
            sqLiteManager.deleteEventsByDateTimeId(matchToMove.getDateTimeId());
        }

        // FINALLY, INSERT THE NEW MATCH INTO BOTH DATABASES
        //This is where the match id is created in order to identify the match in the database
        String dateTimeId = Calendar.getInstance().getTime().toString();

        //Insert new match into mySql
        matchData.setDateTimeId(dateTimeId);
        mySQLManager.sendRequest("enreg", matchData.convertToJSONArray());

        //Insert new match into sqLite
        sqLiteManager.insertMatch(dateTimeId, matchData.getFirstPlayer(), matchData.getSecondPlayer(),
                matchData.getMatchFormat(), matchData.getFormatSet(), matchData.getLongitude(), matchData.getLatitude());
        sqLiteManager.close();

        return matchData;
    }

    public void deleteHistoryRecord(String matchId, Context context) {
        //SQLite
        sqLiteManager = new SQLiteManager(context);

        //First insert events into SQLite
        sqLiteManager.deleteMatch(matchId);
        sqLiteManager.deleteEventsByDateTimeId(matchId);
        sqLiteManager.close();

        //then insert events into MySQL
        mySQLManager = new MySQLManager(context);

        List param = new ArrayList();
        param.add(matchId);
        mySQLManager.sendRequest("deleteMatch", new JSONArray(param));
        mySQLManager.sendRequest("deleteEvent", new JSONArray(param));
    }

    /**
     * Insert a list of events into the databases
     * @param matchId
     * @param eventDataList
     * @param context
     */
    public void insertEventDataList(String matchId, List<EventData> eventDataList, Context context) {
        //SQLite
        sqLiteManager = new SQLiteManager(context);

        //First insert events into SQLite
        sqLiteManager.insertEvents(matchId, eventDataList);
        sqLiteManager.close();

        //then insert events into MySQL
        mySQLManager = new MySQLManager(true);
        for (EventData eventData : eventDataList) {
            mySQLManager.sendRequest("enregEvent", eventData.convertToJSONArray());
        }
    }

    /**
     * Find all events by the id of the match
     * @param matchId
     * @param context
     * @return
     */
    public List<EventData> findAllEventsById(String matchId, Context context) {
        sqLiteManager = new SQLiteManager(context);
        MatchData matchData = null;
        List<EventData> eventList = new ArrayList<>();

        matchData = sqLiteManager.findMatchDataById(matchId);
        if (matchData != null) {
            eventList = sqLiteManager.findAllEventsById(matchId);
            sqLiteManager.close();
            // Inform the activity where the information came from in order to adapt method that it has to use
            PreviousMatchDetailsActivity.fromSqlLite = true;
            return eventList;
        } else {
            PreviousMatchDetailsActivity.fromSqlLite = false;
        }
        return null;
    }

    /**
     * Ask for display to update
     * @param context
     */
    public void askForDisplayUpdate(Context context){
        this.getMatches(context);
    }
}
